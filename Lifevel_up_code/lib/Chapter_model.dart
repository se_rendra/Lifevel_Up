import 'package:flutter/cupertino.dart';
import 'package:Rendra_Lifevel_up/temp/widget_questions_model.dart';

//const Color cardColor = Color(0xFF4993FA);

class Chapter {
  final int id;
  final String chapterName;
  final IconData chapterIcon;
  //final Color chapterColor;
  final List<dynamic> ChapterQuestions;

  Chapter({
    required this.id,
    //required this.chapterColor,
    required this.chapterIcon,
    required this.chapterName,
    required this.ChapterQuestions,
  });
}

final List<Chapter> chapterList = [
  Chapter(
    id: 0,
    //chapterColor: cardColor,
    chapterIcon: CupertinoIcons.zzz,
    chapterName: "Beginner",
    ChapterQuestions: BeginnerQuestionsList,
  ),
  Chapter(
      id: 1,
      //chapterColor: cardColor,
      chapterIcon: CupertinoIcons.square_stack_3d_up,
      chapterName: "Reguler",
      ChapterQuestions: BeginnerQuestionsList),
  Chapter(
    id: 2,
    //chapterColor: cardColor,
    chapterIcon: CupertinoIcons.star,
    chapterName: "Expert",
    ChapterQuestions: BeginnerQuestionsList
  ),
  Chapter(
    id: 3,
    //chapterColor: cardColor,
    chapterIcon: CupertinoIcons.wand_stars_inverse,
    chapterName: "Master",
    ChapterQuestions: BeginnerQuestionsList
  ),
];
