import 'package:flutter/material.dart';

class WidgetQuestion {
  final int id;
  final String text;
  final List<WiidgetOption> options;
  bool isLocked;
  WiidgetOption? selectedWiidgetOption;
  WiidgetOption correctAnswer;

  WidgetQuestion({
    required this.text,
    required this.options,
    this.isLocked = false,
    this.selectedWiidgetOption,
    required this.id,
    required this.correctAnswer,
  });

  WidgetQuestion copyWith() {
    return WidgetQuestion(
      id: id,
      text: text,
      options: options
          .map((option) =>
              WiidgetOption(text: option.text, isCorrect: option.isCorrect))
          .toList(),
      isLocked: isLocked,
      selectedWiidgetOption: selectedWiidgetOption,
      correctAnswer: correctAnswer,
    );
  }
}

class WiidgetOption {
  final String? text;
  final bool? isCorrect;

  const WiidgetOption({
    this.text,
    this.isCorrect,
  });
}

final List<WidgetQuestion> BeginnerQuestionsList = [
  WidgetQuestion(
    text:
        "Sarada is one of 5 quintuplets along with Sarida, Surada, Serada, and Sasada. One day, Surada wants to give away all her candies to her siblings. Surada has 400 candies and wants to distribute them equally. How many candies does Saruda have?",
    options: [
      const WiidgetOption(text: "0", isCorrect: true),
      const WiidgetOption(text: "80", isCorrect: false),
      const WiidgetOption(text: "100", isCorrect: false),
      const WiidgetOption(text: "Unknown", isCorrect: false),
    ],
    id: 0,
    correctAnswer: const WiidgetOption(text: "0", isCorrect: true),
  ),
  WidgetQuestion(
    text:
        "3 + 2 x 4(1+3) = ....?",
    options: [
      const WiidgetOption(text: "18", isCorrect: false),
      const WiidgetOption(text: "19", isCorrect: true),
      const WiidgetOption(text: "29", isCorrect: false),
      const WiidgetOption(text: "91", isCorrect: false),
    ],
    id: 1,
    correctAnswer: const WiidgetOption(text: "19", isCorrect: true),
  ),
  WidgetQuestion(
    text:
        "Ali has 15 apples. He wants to share his apples equally among 5 friends. How many apples will each of Ali's friends receive??",
    options: [
      const WiidgetOption(text: "Four", isCorrect: false),
      const WiidgetOption(text: "Three", isCorrect: true),
      const WiidgetOption(text: "Six", isCorrect: false),
      const WiidgetOption(text: "Ten", isCorrect: false),
    ],
    id: 2,
    correctAnswer: const WiidgetOption(text: "Three", isCorrect: true),
  ),
  WidgetQuestion(
    text:
        "John has 12 marbles. He gives 3 marbles to each of his friends. How many friends does John have?",
    options: [
      const WiidgetOption(text: "Three", isCorrect: false),
      const WiidgetOption(text: "Four", isCorrect: true),
      const WiidgetOption(text: "Five", isCorrect: false),
      const WiidgetOption(text: "Six", isCorrect: false),
    ],
    id: 3,
    correctAnswer: const WiidgetOption(text: "Four", isCorrect: true),
  ),
  WidgetQuestion(
    text:
        "A farmer has 24 eggs. He puts them into cartons that can each hold 6 eggs. How many cartons does the farmer need?",
    options: [
      const WiidgetOption(text: "Two", isCorrect: false),
      const WiidgetOption(text: "Three", isCorrect: false),
      const WiidgetOption(text: "Four", isCorrect: true),
      const WiidgetOption(text: "Six", isCorrect: false),
    ],
    id: 4,
    correctAnswer: const WiidgetOption(text: "Four", isCorrect: true),
  ),
  // Add more questions similarly
];

class QuizScreen extends StatefulWidget {
  final List<WidgetQuestion> questions;

  const QuizScreen({Key? key, required this.questions}) : super(key: key);

  @override
  _QuizScreenState createState() => _QuizScreenState();
}

class _QuizScreenState extends State<QuizScreen> {
  int _currentIndex = 0;
  int _score = 0;
  bool _showScore = false;

  void _resetQuiz() {
    setState(() {
      _currentIndex = 0;
      _score = 0;
      _showScore = false;
      widget.questions.forEach((question) {
        question.isLocked = false;
        question.selectedWiidgetOption = null;
      });
    });
  }

  void _checkAnswer(WiidgetOption selectedOption) {
    setState(() {
      widget.questions[_currentIndex].selectedWiidgetOption = selectedOption;
      widget.questions[_currentIndex].isLocked = true;

      if (selectedOption.isCorrect!) {
        _score++;
      }
    });
  }

  void _nextQuestion() {
    setState(() {
      if (_currentIndex < widget.questions.length - 1) {
        _currentIndex++;
      } else {
        // Show score or navigate to a different screen, etc.
        // For now, let's just show the score
        setState(() {
          _showScore = true;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    WidgetQuestion currentQuestion = widget.questions[_currentIndex];

    if (_showScore) {
      double percentage = (_score / widget.questions.length) * 100;

      String imageAsset;
      if (percentage < 75) {
        imageAsset = 'assets/1.png';
      } else if (percentage >= 75 && percentage < 85) {
        imageAsset = 'assets/B.gif';
      } else {
        imageAsset = 'assets/C.gif';
      }

      return Scaffold(
        appBar: AppBar(
          title: Text('Quiz Score'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Your Score: ${percentage.toStringAsFixed(0)}',
                style: TextStyle(fontSize: 27),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 10),
              Text(
                '$_score out of ${widget.questions.length}',
                style: TextStyle(fontSize: 20),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              Image.asset(
                imageAsset,
                height: 400,
                width: 400,
                fit: BoxFit.contain,
              ),
              SizedBox(height: 20),
              ElevatedButton(
                onPressed: _resetQuiz,
                child: Text('Restart Quiz'),
              ),
            ],
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Quiz'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Question ${_currentIndex + 1} of ${widget.questions.length}',
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 10),
            Text(
              currentQuestion.text,
              style: TextStyle(fontSize: 20),
            ),
            SizedBox(height: 20),
            Column(
              children: currentQuestion.options.map((option) {
                Color? optionColor;
                IconData? optionIcon;

                if (currentQuestion.isLocked) {
                  if (option == currentQuestion.selectedWiidgetOption) {
                    optionColor = option.isCorrect! ? Colors.green : Colors.red;
                    optionIcon = option.isCorrect!
                        ? Icons.check_circle
                        : Icons.cancel;
                  } else if (option.isCorrect!) {
                    optionColor = Colors.green;
                    optionIcon = Icons.check_circle;
                  }
                }

                return Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: optionColor ?? Colors.blue,
                      foregroundColor: Colors.white,
                      disabledBackgroundColor: optionColor ?? Colors.blue,
                      disabledForegroundColor: Colors.white,
                    ),
                    onPressed: currentQuestion.isLocked
                        ? null
                        : () => _checkAnswer(option),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(option.text!),
                        if (optionIcon != null)
                          Icon(
                            optionIcon,
                            color: Colors.white,
                          ),
                      ],
                    ),
                  ),
                );
              }).toList(),
            ),
            if (currentQuestion.isLocked)
              Align(
                alignment: Alignment.centerRight,
                child: ElevatedButton(
                  onPressed: _nextQuestion,
                  child: Text(
                      _currentIndex < widget.questions.length - 1 ? 'Next Question' : 'Finish'),
                ),
              ),
          ],
        ),
      ),
    );
  }
}

