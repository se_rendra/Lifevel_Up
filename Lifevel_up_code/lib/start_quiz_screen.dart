import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:Rendra_Lifevel_up/quiz_screen.dart'; // Adjust the import path

class Start extends StatefulWidget {
  final String topicName;
  final List<dynamic> typeOfTopic;

  const Start({super.key, required this.topicName, required this.typeOfTopic});

  @override
  State<Start> createState() => _NewCardState();
}

class _NewCardState extends State<Start> {
  String getImagePath(String topicName) {
    switch (topicName.toLowerCase()) {
      case 'beginner':
        return 'assets/Beginner_Logo.gif';
      case 'reguler': 
        return 'assets/Reguler_logo.gif';
      case 'expert':
        return 'assets/Expert_Logo.gif';
      case 'master':
        return 'assets/Master_Logo.gif';
      default:
        return 'assets/Master_Logo.png';
    }
  }

  @override
  Widget build(BuildContext context) {
    const Color bgColor3 = Color.fromARGB(255, 233, 189, 78);

    return Scaffold(
      backgroundColor: bgColor3,
      body: SafeArea(
        child: Center(
          child: ListView(
            physics: const AlwaysScrollableScrollPhysics(),
            padding: const EdgeInsets.all(16),
            children: [
              const SizedBox(
                height: 10,
              ),
              Container(
                padding: const EdgeInsets.only(right: 18.0),
                alignment: Alignment.topCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: const Icon(
                        CupertinoIcons.backward,
                        color: Colors.white,
                        weight: 13,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 50,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    getImagePath(widget.topicName),
                    height: 300,
                  ),
                  const SizedBox(height: 20),
                  Text(
                    widget.topicName,
                    style: const TextStyle(
                      fontSize: 24,
                      color: Colors.white,
                    ),
                  ),
                  const SizedBox(height: 20),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => QuizScreen(
                            questions: BeginnerQuestionsList,
                          ),
                        ),
                      );
                    },
                    child: Text('Start Quiz on ${widget.topicName}'),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
